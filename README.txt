Portfolio Showcase installation profile
=========================================
Portfolio Showcase is a multifaceted installation profile, including a theme and a custom views created specifically for designers and other creatives, looking to showcase their work. The fluid design accompanied by a dynamic content module create an engaging site experience. The installation profile is highly user-configurable with a unique portfolio functionality and is compatible with Drupal 7.

* Cross-Browser/Platform checked: Safari 4+, Firefox 3+, IE7+, and Chrome
* Built specifically for portfolio management and showcasing
* Dynamic mantle with jQuery transitions
* Two-column layout
* Highly user-configurable with 11 content regions
* SEO semantic coding
* Custom built plugins for the portfolio display
* Supports both fluid and fixed layout widths
* Print stylesheet
* Drupal 7 functionality
* CSS based (tableless)


Installation notes
=========================================
* Requires a modern Linux/Apache/MySQL/PHP hosting environment.
* Create a MySQL database for the site.
* Extract this archive into the web root (e.g. htdocs/example.com).
* Connect to the directory into which this site has been extracted.
* Load the site in a browser, an installation screen will display
  - select the Showcase profile (third radio button) and click the Install button
  - enter database information on the next screen; if your host's MySQL server is not the same as the web server, you'll need to open the Advanced section to specify the database server location
  
* Click Install -- installation will proceed, then a "Configure site" page will appear
  - enter the requested info -- this step creates the default account (user 1) which has permission to administer anything on the site

* After installation completes, you'll see a page "Showcase installation complete"

* Visit the home page. To get started, add a new "project" content.
