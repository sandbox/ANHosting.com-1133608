<?php if ($rows): ?>
<div id="hp-mantle-left" class="grid_1 column mantle-arrow">
	<a class="prev browse left">
		<img src="<?php echo $directory; ?>/images/mantle-left.png" class="png" alt="Scroll Left" />
	</a>
</div>
<div id="mantle-image" class="grid_5 column">
	<div id="computer-wrapper">
		<div id="computer" class="png">
      <div class="view-content">
        <?php print $rows; ?>
      </div>
			<div class="light png"></div>
		</div>
	</div>
</div>
<?php elseif ($empty): ?>
  <div class="view-empty">
    <?php print $empty; ?>
  </div>
<?php endif; ?>

