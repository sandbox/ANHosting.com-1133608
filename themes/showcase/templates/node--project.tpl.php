<div class="post">
	<div class="row meta">
		<div class="column grid_7">
			<div class="tags">
				<?php print render($content['field_tags']); ?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="row body">
		<div class="column grid_8">
			<?php print render($content['field_image']); ?>
      <?php print render($content['body']); ?>
		</div>
	</div>
	<div class="row footer">
		<div class="column grid_8">
		</div>
	</div>
</div>