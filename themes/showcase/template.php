<?php

function showcase_preprocess_page(&$vars) {
  if (!module_exists('views')) {
    // This is how I'll manage dependencies
    debug('Please install the Views module');
  }
  $page = $vars['page'];

  // Load the latest 3 nodes of type 'project'
  $query = db_query("SELECT nid FROM {node} WHERE type = :type LIMIT 3",  array(':type' => 'project'));
  $result = $query->fetchCol();
  foreach($result as $node) {
    $vars['projects'][] = node_view(node_load($node));
  }
}

function showcase_preprocess_node(&$vars) {
  // Grab the node object.
  $node = $vars['node'];
  // Make individual variables for the parts of the date.
  $vars['date_day'] = format_date($node->created, 'custom', 'j');
  $vars['date_month'] = format_date($node->created, 'custom', 'M');
  $vars['date_year'] = format_date($node->created, 'custom', 'Y');
  $vars['date_string'] = format_date($node->created, 'custom', 'F jS, o');
  if ($node->type == 'article') {
    if (!$vars['is_front']) {
      $vars['show_title'] = true;
    }
  }
  print_r(variable_get('node_options_article'));
}

function showcase_preprocess_comment(&$vars) {
  $comment = $vars['elements']['#comment'];
  $vars['author'] = theme('username', array('account' => $comment));
  $vars['date_day'] = format_date($comment->created, 'custom', 'j');
  $vars['date_month'] = format_date($comment->created, 'custom', 'M');
  $vars['date_year'] = format_date($comment->created, 'custom', 'Y');
  $vars['date_string'] = format_date($comment->created, 'custom', 'F jS, o') . " at " . format_date($comment->created, 'custom', 'g:h a');
}

function showcase_preprocess_search_result(&$vars) {
  $result = $vars['result'];
  print_r($result['user']);
  $vars['author'] = $result['user'];
  $vars['date_day'] = format_date($result['date'], 'custom', 'j');
  $vars['date_month'] = format_date($result['date'], 'custom', 'M');
  $vars['date_year'] = format_date($result['date'], 'custom', 'Y');
  $vars['date_string'] = format_date($result['date'], 'custom', 'F jS, o') . " at " . format_date($result->created, 'custom', 'g:h a');
}

function showcase_theme() {
  return array(
    'comment_form' => array(
      'arguments' => array('form' => null),
    ),
  );
}

function showcase_comment_form(&$form) {
}